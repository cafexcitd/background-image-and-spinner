angular.module('MyApp',['ngMaterial', 'ngMessages'])

.controller('AppCtrl', function($scope, $window, $mdMedia) {
  resizeProgress();
  
  angular.element($window).bind("resize", function() {
    resizeProgress();
    $scope.$apply();
  });
  
  function resizeProgress () {
     if ($mdMedia("gt-xs")) {
        $scope.diameter = 200;
      }
      else if ($mdMedia("xs")) {
        $scope.diameter = 100;
      }
  }
});
